import os
import json

current_directory = os.path.dirname(os.path.realpath(__file__))
config_file_name = os.path.join(current_directory, u'config.py')
config = json.loads(open(config_file_name).read())


class ISONameConverter(object):
    @staticmethod
    def to_russian(country_code):
        return config[u'to_russian'].get(country_code)

    @staticmethod
    def to_english(country_code):
        return config[u'to_english'].get(country_code)