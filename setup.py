from os.path import join, dirname

from setuptools import setup


setup(
    name='iso_countries',
    version=0.1,
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    author='',
    url='https://bitbucket.org/rebranch/rebranch-iso-countries',
)