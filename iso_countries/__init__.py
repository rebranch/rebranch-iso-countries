import os
import xml.etree.ElementTree as ET
import urllib2
import json

current_directory = os.path.dirname(os.path.realpath(__file__))
config_file_name = os.path.join(current_directory, u'config.py')

res = urllib2.urlopen('http://www.artlebedev.ru/tools/country-list/xml/')
country_et = ET.fromstring(res.read())
to_english = {}
to_russian = {}
for country in country_et.findall('country'):
    to_english[country.find('alpha2').text] = country.find('english').text
    to_russian[country.find('alpha2').text] = country.find('name').text
config = {
    u'to_russian': to_russian,
    u'to_english': to_english
}
with open(config_file_name, 'w') as config_file:
    config_file.write(json.dumps(config))

from .utils import *